# Blog API

## Installation

```
composer install
```

### Migrate Database

```
php artisan migrate
```

### Install Laravel Passport

```
php artisan passport:install
```